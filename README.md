# Building

The application uses Maven as it's build and dependency manager. To build the application, change to the `appdirectchallenge-parent` directory and run the
standard Maven commands such as:

> mvn clean install

# Running

To run the application, change to the `appdirectchallenge-service/target` directory and run:

> java -jar appdirectchallenge-service-1.0.0-SNAPSHOT.jar

# Configuring

The only configuration parameters that should currently be changed by the application are:

* oauth.consumerKey: the OAuth consumer key to use when signing and validating signed URLs
* oauth.consumerSecret: the OAuth consumer secret to use when signing and validating signed URLs
* spring.datasource.url: the URL to the MySQL instance and database to use as storage backend

The configuration is currently "built into" the application.  It is stored as a resource in the service jar file.  However, using the standard Spring mechanisms,
we could write an override YAML or properties file, and instruct Spring to use it in addition to the default configuration file stored in hte service jar.  To do so,
use the --spring-config-location argument when running the application.  For example:

> java -jar appdirectchallenge-service-1.0.0-SNAPSHOT.jar --spring-config-location override.properties

With an override.properties file like:

>>>
oauth.consumerKey: myproject-123456

oauth.consumerSecret: HD28W40dJ9W9u6wr

spring.datasource.url: jdbc:mysql://myhost:3306/myproject
>>>

# Design

The design of the backend is to use a single endpoint (notification URL) for all marketplace events.  The endpoint is implemented by the `NotificationService` Spring
REST controller.

The endpoint in turn uses a `NotificationEventDispatcher` to dispatch events to `INotificationHandler` instances.  The notification handler instances are automatically
discovered and registered with the dispatcher when the application starts.  The automatic discovery is achieved by declaring all `INotificationHandler` instances as beans
and using the standard Spring mechanism to obtain all such beans and add them to the dispatcher.

Data is persisted to a MySQL database via a Spring Data JPA repository implementation.  Notification handlers currently talk directly to the repository API in order to
store and retrieve data.  The repository implementation is auto-configured using Spring Boot's autoconfiguration facility.  This means that if we have multiple repository
implementations for different data backends, all we need to do to switch between them is to switch the implementation dependency in Maven.  Note that, as mentioned below,
the repository API should be factored out of the current implementation and shared across all implementations in this case.

# Next steps

If I were to continue working on this project, I would perform the following:

* Handle many more exceptional cases and return the proper error codes, as specified in the documentation.
* Add logging (there is almost none).
* Add unit tests
* Add additional notification handlers for the other possible events (user assignment, add-ons, subscription notices, etc.)
* Add an additional repository implementation to show how easy it is to do so. In doing so, I would split the repository API into a separate module from the implementations.
* Add data model validation (used Hibernate validation constraints). These can be used regardless of the underlying repository implementation chosen.
* Use a Spring Security OAuth filter to validate incoming signed requests instead of the manual validation I implemented.
* Consider writing additional "data services" to be used between the notification handlers and the repositories.  This depends on the amount of code repetition there
  is between the handlers.  The idea is that validation and other repeated tasks such as "lookup or save if not exists" could be done at the data service level.

# Things I'm unhappy with

* This is a typical issue with data models but there is often a different representation within the database than the data that is transferred between services.
  Perhaps the data model could be divided into "data transfer" and "data definition" objects.  If this is done, something like the commons-beans
  `PropertyUtils.copyProperties()` could be used to copy identical properties from one representation to the other.  Currently, using the same objects for storage and
  transfer is ok.
* What I really don't like though is the JPA/Hibernate implementation via Java annotations.  The annotations are specific to JPA/Hibernate and don't apply to other
  repository implementations.  Ideally, we would use another mechanism (XML?) to specify
  the JPA/Hibernate mappings and that mechanism would be contained in a jpa-specific repository implementation module.  This way, another implementation such as MongoGB
  can rely on the same data model objects, without the additional JPA/Hibernate-specific annotations.

# Limitations

Currently, only one AppDirect Marketplace project is supported since only one OAuth consumer key/secret combination may be specified and the data model and repositories
don't consider multiple projects.  We could easily support multiple OAuth credentials by specifying them as a list.  As for storing data for multiple projects in the backend,
we could do so either by using one database per project or adding a project identifier to each data model object.
