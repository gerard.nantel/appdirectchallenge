package com.gerardnantel.appdirectchallenge.repository.subscription;

import com.gerardnantel.appdirectchallenge.api.subscription.Order;
import org.springframework.data.repository.CrudRepository;

public interface IOrderRepository extends CrudRepository<Order, String> {
}
