package com.gerardnantel.appdirectchallenge.repository;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories
public class RepositoryAutoConfiguration {
}
