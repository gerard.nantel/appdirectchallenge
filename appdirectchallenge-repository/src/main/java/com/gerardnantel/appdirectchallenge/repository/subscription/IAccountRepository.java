package com.gerardnantel.appdirectchallenge.repository.subscription;

import com.gerardnantel.appdirectchallenge.api.subscription.Account;
import org.springframework.data.repository.CrudRepository;

public interface IAccountRepository extends CrudRepository<Account, String> {
    Account findById(String id);

    Account findByCompanyId(String companyId);
}
