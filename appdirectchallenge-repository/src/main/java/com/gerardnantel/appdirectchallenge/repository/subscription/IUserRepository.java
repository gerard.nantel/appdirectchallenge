package com.gerardnantel.appdirectchallenge.repository.subscription;

import com.gerardnantel.appdirectchallenge.api.User;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface IUserRepository extends CrudRepository<User, String> {
    User findByEmail(String email);
}
