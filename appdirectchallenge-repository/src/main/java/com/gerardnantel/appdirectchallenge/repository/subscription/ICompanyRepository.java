package com.gerardnantel.appdirectchallenge.repository.subscription;

import com.gerardnantel.appdirectchallenge.api.subscription.Company;
import org.springframework.data.repository.CrudRepository;

public interface ICompanyRepository extends CrudRepository<Company, String> {
    Company findById(String uuid);
}
