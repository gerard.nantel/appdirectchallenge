package com.gerardnantel.appdirectchallenge.api;

public class FailureResponse extends AbstractResponse {
    private final String errorCode;
    private final String message;

    public FailureResponse(String errorCode, String message) {
        super(false);

        this.errorCode = errorCode;
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }
}
