package com.gerardnantel.appdirectchallenge.api;

import java.net.URI;

public class Marketplace {
    private URI baseUrl;
    private String partner;

    public URI getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(URI baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }
}
