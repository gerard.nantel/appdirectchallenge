package com.gerardnantel.appdirectchallenge.api;

public class SuccessResponse extends AbstractResponse {
    public static final SuccessResponse INSTANCE = new SuccessResponse();

    public SuccessResponse() {
        super(true);
    }
}
