package com.gerardnantel.appdirectchallenge.api;

public interface IResponse {
    boolean isSuccess();
}
