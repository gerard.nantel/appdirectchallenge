package com.gerardnantel.appdirectchallenge.api.subscription;

import com.gerardnantel.appdirectchallenge.api.SuccessResponse;

public class SubscriptionSuccessResponse extends SuccessResponse {
    private final String accountIdentifier;

    public SubscriptionSuccessResponse(String accountIdentifier) {
        this.accountIdentifier = accountIdentifier;
    }

    public String getAccountIdentifier() {
        return accountIdentifier;
    }
}
