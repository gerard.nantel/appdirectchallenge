package com.gerardnantel.appdirectchallenge.api;

public abstract class AbstractResponse {
    private final boolean success;

    AbstractResponse(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }
}
