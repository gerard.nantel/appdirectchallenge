package com.gerardnantel.appdirectchallenge.api.event;

import com.gerardnantel.appdirectchallenge.api.Marketplace;
import com.gerardnantel.appdirectchallenge.api.User;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;

public class NotificationEvent {
    private final NotificationEventType type;
    private final Marketplace marketplace;
    private final User creator;
    private final JsonObject payload;

    public NotificationEvent(NotificationEventType type, Marketplace marketplace, User creator, JsonObject payload) {
        this.type = type;
        this.marketplace = marketplace;
        this.creator = creator;
        this.payload = payload;
    }

    public NotificationEventType getType() {
        return type;
    }

    public Marketplace getMarketplace() {
        return marketplace;
    }

    public User getCreator() {
        return creator;
    }

    public JsonObject getPayloadJson() {
        return payload;
    }
}
