package com.gerardnantel.appdirectchallenge.api.event;

public enum NotificationEventType {
    SUBSCRIPTION_ORDER,
    SUBSCRIPTION_CHANGE,
    SUBSCRIPTION_CANCEL
}
