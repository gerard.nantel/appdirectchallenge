package com.gerardnantel.appdirectchallenge.api.subscription;

public class ChangeSubscriptionPayload {
    private Account account;
    private Order order;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
