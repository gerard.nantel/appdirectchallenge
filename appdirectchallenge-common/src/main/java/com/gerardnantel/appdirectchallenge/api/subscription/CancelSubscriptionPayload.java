package com.gerardnantel.appdirectchallenge.api.subscription;

public class CancelSubscriptionPayload {
    private Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
