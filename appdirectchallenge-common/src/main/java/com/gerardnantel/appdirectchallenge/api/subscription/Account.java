package com.gerardnantel.appdirectchallenge.api.subscription;

import com.gerardnantel.appdirectchallenge.api.User;
import com.google.gson.annotations.SerializedName;
import com.sun.istack.internal.NotNull;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "accounts")
public class Account {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @SerializedName("accountIdentifier")
    private String id;

    private String status;

    @NotNull
    @OneToOne//(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private User creator;
    @NotNull
    @OneToOne//(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Company company;
    @OneToOne(mappedBy = "account", cascade = CascadeType.ALL)
    @NotNull
    private Order order;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        order.setAccount(this);
        this.order = order;
    }
}
