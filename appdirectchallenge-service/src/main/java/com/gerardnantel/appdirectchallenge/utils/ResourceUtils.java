package com.gerardnantel.appdirectchallenge.utils;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

public class ResourceUtils {
    public static InputStreamReader getReader(Resource resource) throws IOException {
        return new InputStreamReader(resource.getInputStream());
    }

    // NOTE: This method is inefficient and should be used carefully (in unit tests and tools)
    public static Resource newCallerRelativeResource(String relativePath) {
        try {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            return newClassRelativeResource(Class.forName(stackTrace[2].getClassName()), relativePath);
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    public static Resource newClassRelativeResource(Class<?> baseClass, String relativePath) {
        return newPackageRelativeResource(baseClass.getPackage(), relativePath);
    }

    public static Resource newPackageRelativeResource(Package basePackage, String relativePath) {
        String resourcePath = String.format("%s/%s", basePackage.getName().replace('.', '/'), relativePath);
        return new ClassPathResource(resourcePath);
    }
}
