package com.gerardnantel.appdirectchallenge.utils;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

import javax.servlet.http.HttpServletRequest;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class OAuthParameters {
    public static final String SIGNATURE_PARAMETER_NAME = "oauth_signature";

    private static final String consumerKeyParameterName = "oauth_consumer_key";
    private static final String nonceParameterName = "oauth_nonce";
    private static final String signatureMethodParameterName = "oauth_signature_method";
    private static final String timestampParameterName = "oauth_timestamp";
    private static final String versionParameterName = "oauth_version";

    private final Map<String, String> parametersByName;

    private OAuthParameters(Map<String, String> parametersByName) {
        this.parametersByName = parametersByName;
    }

    public static OAuthParameters fromServletRequest(HttpServletRequest request) {
        String authorizationHeader = request.getHeader("authorization");
        if (authorizationHeader != null) {
            return fromAuthorizationHeader(authorizationHeader);
        }

        return fromRequestParameters(request.getParameterMap());
    }

    public static OAuthParameters fromAuthorizationHeader(String authorizationHeader) {
        try {
            Map<String, String> parametersByName = new HashMap<>();

            authorizationHeader = StringUtils.stripStart(authorizationHeader, "OAuth ");
            String[] parameters = StringUtils.split(authorizationHeader, ", ");
            for (String parameter : parameters) {
                String[] keyAndValue = StringUtils.split(parameter, "=");
                String value = StringUtils.strip(keyAndValue[1], "\"");
                value = URLDecoder.decode(value, "UTF-8");
                parametersByName.put(keyAndValue[0], value);
            }

            return new OAuthParameters(parametersByName);
        } catch (Exception e) {
            throw new IllegalArgumentException("Error extracting OAuth parameters from authorization header", e);
        }
    }

    public static OAuthParameters fromRequestParameters(Map<String, String[]> requestParameters) {
        Map<String, String> oAuthParametersByName = new HashMap<>();

        for (Map.Entry<String, String[]> entry : requestParameters.entrySet()) {
            String key = entry.getKey();
            String[] values = entry.getValue();

            if (ArrayUtils.isEmpty(values)) {
                continue;
            }

            if (key.startsWith("oauth_")) {
                oAuthParametersByName.put(key, values[0]);
            }
        }

        return new OAuthParameters(oAuthParametersByName);
    }

    public String getConsumerKey() {
        return getParameter(consumerKeyParameterName);
    }

    public String getNonce() {
        return getParameter(nonceParameterName);
    }

    public String getSignature() {
        return getParameter(SIGNATURE_PARAMETER_NAME);
    }

    public String getSignatureMethod() {
        return getParameter(signatureMethodParameterName);
    }

    public long getTimestamp() {
        String timestamp = getParameter(timestampParameterName);
        return Long.parseLong(timestamp);
    }

    public String getVersion() {
        return getParameter(versionParameterName);
    }

    private String getParameter(String name) {
        String value = parametersByName.get(name);
        Validate.isTrue(StringUtils.isNotBlank(value), "Missing expected authorization parameter %s", name);
        return value;
    }

    public Map<String, String> asMap() {
        return parametersByName;
    }
}
