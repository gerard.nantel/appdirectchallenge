package com.gerardnantel.appdirectchallenge.utils;

import oauth.signpost.OAuthConsumer;
import org.apache.commons.lang3.Validate;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URIBuilder;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

public class OAuthValidator {
    private static final String expectedSignatureMethod = "HMAC-SHA1";
    private static final String expectedVersion = "1.0";

    private final OAuthConsumer oAuthConsumer;
    private final String expectedConsumerKey;
    private final Set<String> noncesUsed = new ConcurrentSkipListSet<>();
    private long currentTimestamp;

    public OAuthValidator(OAuthConsumer oAuthConsumer, String expectedConsumerKey) {
        this.oAuthConsumer = oAuthConsumer;
        this.expectedConsumerKey = expectedConsumerKey;
    }

    public void validate(HttpServletRequest request) {
        OAuthParameters parameters = OAuthParameters.fromServletRequest(request);
        validateSignatureMethod(parameters);
        validateConsumerKey(parameters);
        validateVersion(parameters);
        validateTimestamp(parameters);
        validateNonce(parameters);
        validateSignature(request, parameters);
    }

    private void validateSignatureMethod(OAuthParameters parameters) {
        validateParameter(parameters.getSignatureMethod(), expectedSignatureMethod);
    }

    private void validateConsumerKey(OAuthParameters parameters) {
        validateParameter(parameters.getConsumerKey(), expectedConsumerKey);
    }

    private void validateVersion(OAuthParameters parameters) {
        validateParameter(parameters.getVersion(), expectedVersion);
    }

    private synchronized void validateTimestamp(OAuthParameters parameters) {
        long timestamp = parameters.getTimestamp();

        if (currentTimestamp != timestamp) {
            Validate.isTrue(timestamp > currentTimestamp, "New timestamp %d is less than previously used timestamp %d", timestamp, currentTimestamp);
            currentTimestamp = timestamp;
        }

        // NOTE: Could also validate that provided timestamp is not too far in the past
    }

    private void validateNonce(OAuthParameters parameters) {
        String nonce = parameters.getNonce();
        Validate.isTrue(!noncesUsed.contains(nonce), "Provided nonce %s was already used", nonce);
    }

    private void validateSignature(HttpServletRequest request, OAuthParameters parameters) {
        URI uriToSign = determineUriToSign(request, parameters);
        String signedUri = signRequestUri(uriToSign);
        String expectedSignature = extractSignatureFromSignedUri(signedUri);
        String signature = parameters.getSignature();
        Validate.isTrue(signature.equals(expectedSignature), "Provided signature %s does not match expected signature %s", signature, expectedSignature);
    }

    private URI determineUriToSign(HttpServletRequest request, OAuthParameters parameters) {
        URIBuilder builder = new URIBuilder(URI.create(request.getRequestURL().toString()));

        for (Map.Entry<String, String[]> entry : request.getParameterMap().entrySet()) {
            for (String value : entry.getValue()) {
                builder.addParameter(entry.getKey(), value);
            }
        }

        for (Map.Entry<String, String> entry : parameters.asMap().entrySet()) {
            if (!entry.getKey().equals(OAuthParameters.SIGNATURE_PARAMETER_NAME)) {
                builder.addParameter(entry.getKey(), entry.getValue());
            }
        }

        try {
            return builder.build();
        } catch (URISyntaxException e) {
            throw new IllegalStateException("Error building full request URI for signing", e);
        }
    }

    private String signRequestUri(URI uri) {
        try {
            return oAuthConsumer.sign(uri.toString());
        } catch (Exception e) {
            throw new IllegalStateException(MessageFormat.format("Error generating OAuth signature for request URI {0}", uri), e);
        }
    }

    private String extractSignatureFromSignedUri(String signedUri) {
        try {
            List<NameValuePair> queryParams = new URIBuilder(signedUri).getQueryParams();
            for (NameValuePair nameValuePair : queryParams) {
                if (nameValuePair.getName().equals(OAuthParameters.SIGNATURE_PARAMETER_NAME)) {
                    return nameValuePair.getValue();
                }
            }
        } catch (URISyntaxException e) {
            throw new IllegalStateException(MessageFormat.format("Unexpected error parsing signed URI %{0}", signedUri), e);
        }

        throw new IllegalStateException("Could not find signature in signed URI");
    }

    private void validateParameter(String value, String expectedValue) {
        Validate.isTrue(value.equals(expectedValue));
    }
}
