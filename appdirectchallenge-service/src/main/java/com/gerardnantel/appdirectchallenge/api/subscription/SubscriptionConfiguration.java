package com.gerardnantel.appdirectchallenge.api.subscription;

import com.gerardnantel.appdirectchallenge.repository.subscription.IAccountRepository;
import com.gerardnantel.appdirectchallenge.repository.subscription.ICompanyRepository;
import com.gerardnantel.appdirectchallenge.repository.subscription.IOrderRepository;
import com.gerardnantel.appdirectchallenge.repository.subscription.IUserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SubscriptionConfiguration {
    @Bean
    public CreateSubscriptionHandler createSubscriptionHandler(IUserRepository userRepository, ICompanyRepository companyRepository, IOrderRepository orderRepository, IAccountRepository accountRepository) {
        return new CreateSubscriptionHandler(userRepository, companyRepository, orderRepository, accountRepository);
    }

    @Bean
    public ChangeSubscriptionHandler changeSubscriptionHandler(IAccountRepository accountRepository, IOrderRepository orderRepository) {
        return new ChangeSubscriptionHandler(accountRepository, orderRepository);
    }

    @Bean
    public CancelSubscriptionHandler cancelSubscriptionHandler(IAccountRepository accountRepository) {
        return new CancelSubscriptionHandler(accountRepository);
    }
}
