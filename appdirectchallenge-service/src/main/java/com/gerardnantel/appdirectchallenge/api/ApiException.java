package com.gerardnantel.appdirectchallenge.api;

import java.text.MessageFormat;

public class ApiException extends RuntimeException {
    private final String errorCode;

    public ApiException(String errorCode, String format, Object... args) {
        super(MessageFormat.format(format, args));
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
