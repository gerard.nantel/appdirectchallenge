package com.gerardnantel.appdirectchallenge.api;

import com.gerardnantel.appdirectchallenge.api.event.NotificationEventDispatcher;
import com.gerardnantel.appdirectchallenge.api.event.NotificationEventFetcher;
import com.gerardnantel.appdirectchallenge.utils.OAuthValidator;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class NotificationConfiguration {
    @Autowired
    private Environment environment;

    @Bean
    public OAuthValidator oAuthValidator() {
        String consumerKey = environment.getRequiredProperty("oauth.consumerKey");
        return new OAuthValidator(oAuthConsumer(), consumerKey);
    }

    @Bean
    public OAuthConsumer oAuthConsumer() {
        String consumerKey = environment.getRequiredProperty("oauth.consumerKey");
        String consumerSecret = environment.getRequiredProperty("oauth.consumerSecret");
        return new CommonsHttpOAuthConsumer(consumerKey, consumerSecret);
    }

    @Bean
    public NotificationService notificationService(NotificationEventFetcher notificationEventFetcher, NotificationEventDispatcher notificationEventDispatcher) {
        return new NotificationService(oAuthValidator(), notificationEventFetcher, notificationEventDispatcher);
    }
}
