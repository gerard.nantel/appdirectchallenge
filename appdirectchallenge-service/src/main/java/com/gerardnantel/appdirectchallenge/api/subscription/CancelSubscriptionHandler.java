package com.gerardnantel.appdirectchallenge.api.subscription;

import com.gerardnantel.appdirectchallenge.api.ApiException;
import com.gerardnantel.appdirectchallenge.api.SuccessResponse;
import com.gerardnantel.appdirectchallenge.api.event.INotificationEventHandler;
import com.gerardnantel.appdirectchallenge.api.event.NotificationEvent;
import com.gerardnantel.appdirectchallenge.repository.subscription.IAccountRepository;

public class CancelSubscriptionHandler implements INotificationEventHandler<CancelSubscriptionPayload> {
    private final IAccountRepository accountRepository;

    CancelSubscriptionHandler(IAccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public SuccessResponse handle(NotificationEvent notificationEvent, CancelSubscriptionPayload payload) {
        Account account = accountRepository.findById(payload.getAccount().getId());
        if (account == null) {
            throw new ApiException("ACCOUNT_DOES_NOT_EXIST", "No account with id {0} exists", account.getId());
        }
        accountRepository.delete(account);

        return SuccessResponse.INSTANCE;
    }

    private Account updateAccount(ChangeSubscriptionPayload payload) {
        Account account = payload.getAccount();
        String accountStatus = account.getStatus();
        account = accountRepository.findById(account.getId());
        account.setStatus(accountStatus);
        return accountRepository.save(account);
    }

    @Override
    public Class<CancelSubscriptionPayload> getPayloadType() {
        return CancelSubscriptionPayload.class;
    }
}
