package com.gerardnantel.appdirectchallenge.api.event;

import com.google.gson.Gson;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.text.MessageFormat;

public class NotificationEventFetcher {
    private final HttpClient httpClient;
    private final OAuthConsumer oAuthConsumer;
    private final Gson gson;

    public NotificationEventFetcher(HttpClient httpClient, OAuthConsumer oAuthConsumer, Gson gson) {
        this.httpClient = httpClient;
        this.oAuthConsumer = oAuthConsumer;
        this.gson = gson;
    }

    public NotificationEvent fetch(URI uri) {
        HttpGet request = new HttpGet(uri);
        request.setHeader("Accept", "application/json");

        sign(request);

        try {
            return httpClient.execute(request, this::handleDownloadResponse);
        } catch (Exception e) {
            throw new IllegalArgumentException(MessageFormat.format("Error fetching notification event from {0}", uri), e);
        }
    }

    private void sign(HttpGet request) {
        try {
            oAuthConsumer.sign(request);
        } catch (Exception e) {
            throw new IllegalStateException(MessageFormat.format("Error signing request to {}", request.getURI()), e);
        }
    }

    private NotificationEvent handleDownloadResponse(HttpResponse response) throws IOException {
        Reader reader = new InputStreamReader(response.getEntity().getContent());
        return gson.fromJson(reader, NotificationEvent.class);
    }
}
