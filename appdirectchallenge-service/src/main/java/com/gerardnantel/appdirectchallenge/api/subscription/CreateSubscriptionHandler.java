package com.gerardnantel.appdirectchallenge.api.subscription;

import com.gerardnantel.appdirectchallenge.api.ApiException;
import com.gerardnantel.appdirectchallenge.api.SuccessResponse;
import com.gerardnantel.appdirectchallenge.api.User;
import com.gerardnantel.appdirectchallenge.api.event.INotificationEventHandler;
import com.gerardnantel.appdirectchallenge.api.event.NotificationEvent;
import com.gerardnantel.appdirectchallenge.repository.subscription.IAccountRepository;
import com.gerardnantel.appdirectchallenge.repository.subscription.ICompanyRepository;
import com.gerardnantel.appdirectchallenge.repository.subscription.IOrderRepository;
import com.gerardnantel.appdirectchallenge.repository.subscription.IUserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ParameterizedMessageFactory;

public class CreateSubscriptionHandler implements INotificationEventHandler<CreateSubscriptionPayload> {
    private static final Logger log = LogManager.getLogger(ParameterizedMessageFactory.class);

    private final IUserRepository userRepository;
    private final ICompanyRepository companyRepository;
    private final IOrderRepository orderRepository;
    private final IAccountRepository accountRepository;

    CreateSubscriptionHandler(IUserRepository userRepository, ICompanyRepository companyRepository, IOrderRepository orderRepository, IAccountRepository accountRepository) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.orderRepository = orderRepository;
        this.accountRepository = accountRepository;
    }

    @Override
    public SuccessResponse handle(NotificationEvent notificationEvent, CreateSubscriptionPayload payload) {
        log.info("Handling subscription order event");

        Account account = createAccount(notificationEvent.getCreator(), payload.getCompany(), payload.getOrder());

        // NOTE: Normally we should be able to use hibernate cascading persistence and merging but it wasn't working
        // properly for me in all situations so I reverted to manually persisting/updating the account creator and company.
        userRepository.save(account.getCreator());
        companyRepository.save(account.getCompany());

        ensureAccountDoesNotAlreadyExist(account.getCompany());
        account = accountRepository.save(account);

        return new SubscriptionSuccessResponse(account.getId());
    }

    private void ensureAccountDoesNotAlreadyExist(Company company) {
        Account account = accountRepository.findByCompanyId(company.getId());
        if (account != null) {
            // TODO: I don't think this should ever happen since the marketplace should prevent it
            throw new ApiException("ACCOUNT_ALREADY_EXISTS", "There is already an account for this company");
        }
    }

    private Account createAccount(User creator, Company company, Order order) {
        Account account = new Account();
        account.setCreator(creator);
        account.setCompany(company);
        account.setOrder(order);

        if (order.hasItems()) {
            for (Order.Item orderItem : order.getItems()) {
                orderItem.setOrder(order);
            }
        }

        return account;
    }


    @Override
    public Class<CreateSubscriptionPayload> getPayloadType() {
        return CreateSubscriptionPayload.class;
    }
}
