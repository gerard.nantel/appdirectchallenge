package com.gerardnantel.appdirectchallenge.api.event;

import com.gerardnantel.appdirectchallenge.api.SuccessResponse;
import com.gerardnantel.appdirectchallenge.api.event.NotificationEvent;

public interface INotificationEventHandler<T> {
    SuccessResponse handle(NotificationEvent notificationEvent, T payload);

    Class<T> getPayloadType();
}
