package com.gerardnantel.appdirectchallenge.api;

import com.gerardnantel.appdirectchallenge.api.event.NotificationEvent;
import com.gerardnantel.appdirectchallenge.api.event.NotificationEventDispatcher;
import com.gerardnantel.appdirectchallenge.api.event.NotificationEventFetcher;
import com.gerardnantel.appdirectchallenge.utils.OAuthValidator;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.ParameterizedMessageFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;

@RestController
public class NotificationService {
    private static final Logger log = org.apache.logging.log4j.LogManager.getLogger(ParameterizedMessageFactory.INSTANCE);

    private final OAuthValidator oAuthValidator;
    private final NotificationEventFetcher notificationEventFetcher;
    private final NotificationEventDispatcher notificationEventDispatcher;

    NotificationService(OAuthValidator oAuthValidator, NotificationEventFetcher notificationEventFetcher, NotificationEventDispatcher notificationEventDispatcher) {
        this.oAuthValidator = oAuthValidator;
        this.notificationEventFetcher = notificationEventFetcher;
        this.notificationEventDispatcher = notificationEventDispatcher;
    }

    @GetMapping("/api/subscription/notification")
    public AbstractResponse handleNotificationEvent(HttpServletRequest request) {
        try {
            oAuthValidator.validate(request);

            URI uri = URI.create(request.getParameter("url"));
            NotificationEvent event = notificationEventFetcher.fetch(uri);
            return notificationEventDispatcher.dispatch(event);
        } catch (ApiException e) {
            log.error("Error handling notification event", e);
            return new FailureResponse(e.getErrorCode(), e.getMessage());
        } catch (Exception e) {
            log.error("Error handling notification event", e);
            return new FailureResponse("INTERNAL_SERVER_ERROR", e.getMessage());
        }
    }
}
