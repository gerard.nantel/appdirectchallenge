package com.gerardnantel.appdirectchallenge.api.event;

import com.gerardnantel.appdirectchallenge.api.subscription.CancelSubscriptionPayload;
import com.gerardnantel.appdirectchallenge.api.subscription.ChangeSubscriptionPayload;
import com.gerardnantel.appdirectchallenge.api.subscription.CreateSubscriptionPayload;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import oauth.signpost.OAuthConsumer;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class NotificationEventConfiguration {
    @Autowired
    private Environment environment;

    @Bean
    public NotificationEventFetcher notificationFetcher(OAuthConsumer oAuthConsumer) {
        return new NotificationEventFetcher(httpClient(), oAuthConsumer, gson());
    }

    @Bean
    public HttpClient httpClient() {
        return HttpClients.createDefault();
    }

    @Bean
    public NotificationEventDispatcher notificationEventDispatcher(ApplicationContext applicationContext) {
        // This mapping is not desirable because it makes the implementation more fragile. We have to remember to add a
        // mapping each time a new event is added.  An alternative would be to have event handlers return both the
        // event type adn the payload or associate the event type to the payload in a standard way such as a public
        // static variable. It's not the end of the world, since it would result in an error when a mapping is missing.
        Map<Class<?>, NotificationEventType> eventTypesByPayloadType = new HashMap<>();
        eventTypesByPayloadType.put(CreateSubscriptionPayload.class, NotificationEventType.SUBSCRIPTION_ORDER);
        eventTypesByPayloadType.put(ChangeSubscriptionPayload.class, NotificationEventType.SUBSCRIPTION_CHANGE);
        eventTypesByPayloadType.put(CancelSubscriptionPayload.class, NotificationEventType.SUBSCRIPTION_CANCEL);
        return new NotificationEventDispatcher(applicationContext, gson(), eventTypesByPayloadType);
    }

    @Bean
    public Gson gson() {
        return new GsonBuilder().create();
    }
}
