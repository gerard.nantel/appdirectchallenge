package com.gerardnantel.appdirectchallenge.api.subscription;

import com.gerardnantel.appdirectchallenge.api.ApiException;
import com.gerardnantel.appdirectchallenge.api.SuccessResponse;
import com.gerardnantel.appdirectchallenge.api.event.INotificationEventHandler;
import com.gerardnantel.appdirectchallenge.api.event.NotificationEvent;
import com.gerardnantel.appdirectchallenge.repository.subscription.IAccountRepository;
import com.gerardnantel.appdirectchallenge.repository.subscription.IOrderRepository;

public class ChangeSubscriptionHandler implements INotificationEventHandler<ChangeSubscriptionPayload> {
    private final IAccountRepository accountRepository;
    private final IOrderRepository orderRepository;

    ChangeSubscriptionHandler(IAccountRepository accountRepository, IOrderRepository orderRepository) {
        this.accountRepository = accountRepository;
        this.orderRepository = orderRepository;
    }

    @Override
    public SuccessResponse handle(NotificationEvent notificationEvent, ChangeSubscriptionPayload payload) {
        Account account = updateAccount(payload);
        updateOrder(account, payload);

        return SuccessResponse.INSTANCE;
    }

    private Account updateAccount(ChangeSubscriptionPayload payload) {
        Account payloadAccount = payload.getAccount();
        String accountStatus = payloadAccount.getStatus();

        Account account = accountRepository.findById(payloadAccount.getId());
        if (account == null) {
            throw new ApiException("ACCOUNT_DOES_NOT_EXIST", "No account with id {0} exists", payloadAccount.getId());
        }

        account.setStatus(accountStatus);

        return accountRepository.save(account);
    }

    private void updateOrder(Account account, ChangeSubscriptionPayload payload) {
        Order existingOrder = account.getOrder();

        Order updatedOrder = payload.getOrder();

        existingOrder.setItems(updatedOrder.getItems());
        existingOrder.setPricingDuration(updatedOrder.getPricingDuration());
        existingOrder.setEditionCode(updatedOrder.getEditionCode());
        existingOrder.setItems(updatedOrder.getItems());

        if (existingOrder.hasItems()) {
            for (Order.Item orderItem : existingOrder.getItems()) {
                orderItem.setOrder(existingOrder);
            }
        }

        orderRepository.save(existingOrder);
    }


    @Override
    public Class<ChangeSubscriptionPayload> getPayloadType() {
        return ChangeSubscriptionPayload.class;
    }
}
