package com.gerardnantel.appdirectchallenge.api.event;

import com.gerardnantel.appdirectchallenge.api.SuccessResponse;
import com.google.gson.Gson;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class NotificationEventDispatcher implements InitializingBean {
    private final ApplicationContext applicationContext;
    private final Gson gson;
    private final Map<Class<?>, NotificationEventType> eventTypesByPayloadType;

    private Map<NotificationEventType, INotificationEventHandler<?>> handlersByType = new HashMap<>();

    NotificationEventDispatcher(ApplicationContext applicationContext, Gson gson, Map<Class<?>, NotificationEventType> eventTypesByPayloadType) {
        this.applicationContext = applicationContext;
        this.gson = gson;
        this.eventTypesByPayloadType = eventTypesByPayloadType;
    }

    public SuccessResponse dispatch(NotificationEvent notificationEvent) {
        INotificationEventHandler<?> handler = getHandlerForType(notificationEvent.getType());
        return invokeNotificationHandler(handler, notificationEvent);
    }

    private <T> SuccessResponse invokeNotificationHandler(INotificationEventHandler<T> handler, NotificationEvent notificationEvent) {
        Class<T> payloadType = handler.getPayloadType();
        T payload = gson.fromJson(notificationEvent.getPayloadJson(), payloadType);
        return handler.handle(notificationEvent, payload);
    }

    private INotificationEventHandler getHandlerForType(NotificationEventType type) {
        INotificationEventHandler handler = handlersByType.get(type);
        if (handler == null) {
            throw new IllegalStateException(MessageFormat.format("Unexpected notification event handler type {0}", type));
        }
        return handler;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Collection<INotificationEventHandler> handlers = applicationContext.getBeansOfType(INotificationEventHandler.class).values();
        for (INotificationEventHandler handler : handlers) {
            Class<?> payloadType = handler.getPayloadType();
            NotificationEventType eventType = eventTypesByPayloadType.get(payloadType);
            addHandler(eventType, handler);
        }
    }

    private void addHandler(NotificationEventType type, INotificationEventHandler handler) {
        handlersByType.put(type, handler);
    }
}
