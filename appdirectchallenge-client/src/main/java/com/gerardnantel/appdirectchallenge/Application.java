package com.gerardnantel.appdirectchallenge;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public AppDirectClientTester tester(Notifier notifier) {
        return new AppDirectClientTester(notifier);
    }

    @Bean
    public Notifier notifier(EventService eventService) {
        return new Notifier(httpClient(), oAuthConsumer(), eventService, gson());
    }

    @Bean
    public HttpClient httpClient() {
        return HttpClients.createDefault();
    }

    @Bean
    public OAuthConsumer oAuthConsumer() {
        return new CommonsHttpOAuthConsumer("gerardtest-171158", "HD28W40dJ9W9u6wr");
    }

    @Bean
    public Gson gson() {
        return new GsonBuilder().create();
    }
}
