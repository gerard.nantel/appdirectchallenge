package com.gerardnantel.appdirectchallenge;

import com.gerardnantel.appdirectchallenge.api.event.NotificationEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RestController
public class EventService {
    private Map<String, NotificationEvent> eventsById = new HashMap<>();

    @GetMapping("/events/{eventId}")
    public NotificationEvent getEvent(HttpServletRequest request, @PathVariable("eventId") String eventId) {
        NotificationEvent event = eventsById.remove(eventId);
        if (event == null) {
            // TODO: Return 404
        }
        return event;
    }

    void stageEvent(String id, NotificationEvent event) {
        eventsById.put(id, event);
    }
}
