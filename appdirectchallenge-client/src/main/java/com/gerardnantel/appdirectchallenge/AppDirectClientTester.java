package com.gerardnantel.appdirectchallenge;

import org.springframework.boot.CommandLineRunner;

public class AppDirectClientTester implements CommandLineRunner {
    private Notifier notifier;

    AppDirectClientTester(Notifier notifier) {
        this.notifier = notifier;
    }

    @Override
    public void run(String... args) throws Exception {
        switch (args[0]) {
            case "create":
                notifier.createSubscription();
                break;
            case "change":
                notifier.changeSubscription();
                break;
            case "cancel":
                notifier.cancelSubscription();
                break;
        }
    }
}
