package com.gerardnantel.appdirectchallenge;

import com.gerardnantel.appdirectchallenge.api.Marketplace;
import com.gerardnantel.appdirectchallenge.api.SuccessResponse;
import com.gerardnantel.appdirectchallenge.api.User;
import com.gerardnantel.appdirectchallenge.api.event.NotificationEvent;
import com.gerardnantel.appdirectchallenge.api.event.NotificationEventType;
import com.gerardnantel.appdirectchallenge.api.subscription.Account;
import com.gerardnantel.appdirectchallenge.api.subscription.CancelSubscriptionPayload;
import com.gerardnantel.appdirectchallenge.api.subscription.ChangeSubscriptionPayload;
import com.gerardnantel.appdirectchallenge.api.subscription.Company;
import com.gerardnantel.appdirectchallenge.api.subscription.CreateSubscriptionPayload;
import com.gerardnantel.appdirectchallenge.api.subscription.Order;
import com.gerardnantel.appdirectchallenge.api.subscription.SubscriptionSuccessResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import oauth.signpost.OAuthConsumer;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Collections;
import java.util.UUID;

/**
 * Quickly written class used to test the backend service.
 */
class Notifier {
    private static final URI baseNotificationUri = URI.create("http://localhost:8080/api/subscription/notification");
    private static final URI baseEventServiceUri = URI.create("http://localhost:8081/events/");
    private static final Marketplace marketplace = createMarketplace();
    private static final User creator = createCreator();
    private static final CreateSubscriptionPayload createSubscriptionPayload = createCreateSubscriptionPayload();

    private final HttpClient httpClient;
    private final OAuthConsumer oAuthConsumer;
    private final EventService eventService;
    private final Gson gson;

    private String accountId = "c0b3a3b4-dcee-4553-9998-fd218746a70b";

    Notifier(HttpClient httpClient, OAuthConsumer oAuthConsumer, EventService eventService, Gson gson) {
        this.httpClient = httpClient;
        this.oAuthConsumer = oAuthConsumer;
        this.eventService = eventService;
        this.gson = gson;
    }

    private static Marketplace createMarketplace() {
        Marketplace marketplace = new Marketplace();
        marketplace.setBaseUrl(URI.create("https://marketplace.appdirect.com"));
        marketplace.setPartner("APPDIRECT");
        return marketplace;
    }

    private static User createCreator() {
        User user = new User();
        user.setEmail("gerard.nantel@outlook.com");
        user.setUuid("32645304-62bc-4f54-be13-1be926434fef");
        return user;
    }

    private static CreateSubscriptionPayload createCreateSubscriptionPayload() {
        CreateSubscriptionPayload createSubscriptionPayload = new CreateSubscriptionPayload();
        createSubscriptionPayload.setCompany(createCompany());
        createSubscriptionPayload.setOrder(createOrder());
        return createSubscriptionPayload;
    }

    private static Company createCompany() {
        Company company = new Company();
        company.setCountry("US");
        company.setName("pdrslv");
        company.setUuid("32645304-62bc-4f54-be13-1be926434feb");
        company.setWebsite("outlook.com");
        return company;
    }

    private static Order createOrder() {
        Order order = new Order();
        order.setEditionCode("FREE");
        order.setPricingDuration("MONTHLY");

        Order.Item item = new Order.Item();
        item.setUnit("USER");
        item.setQuantity(4);
        order.setItems(Collections.singletonList(item));

        return order;
    }

    private static Order createUpdatedOrder() {
        Order order = new Order();
        order.setEditionCode("DME");
        order.setPricingDuration("DAILY");
        return order;
    }

    void createSubscription() {
        NotificationEvent event = new NotificationEvent(NotificationEventType.SUBSCRIPTION_ORDER, marketplace, creator, gson.toJsonTree(createSubscriptionPayload).getAsJsonObject());
        SubscriptionSuccessResponse response = sendNotificationEvent(event, SubscriptionSuccessResponse.class);
        accountId = response.getAccountIdentifier();
    }

    void changeSubscription() {
        JsonObject payload = gson.toJsonTree(createChangeSubscriptionPayload()).getAsJsonObject();
        NotificationEvent event = new NotificationEvent(NotificationEventType.SUBSCRIPTION_CHANGE, marketplace, creator, payload);
        SuccessResponse response = sendNotificationEvent(event, SuccessResponse.class);
        response.isSuccess();
    }

    private ChangeSubscriptionPayload createChangeSubscriptionPayload() {
        ChangeSubscriptionPayload changeSubscriptionPayload = new ChangeSubscriptionPayload();
        changeSubscriptionPayload.setAccount(createAccount());
        changeSubscriptionPayload.setOrder(createUpdatedOrder());
        return changeSubscriptionPayload;
    }

    private Account createAccount() {
        Account account = new Account();
        account.setId(accountId);
        account.setStatus("ACTIVE");
        return account;
    }

    void cancelSubscription() {
        JsonObject payload = gson.toJsonTree(createCancelSubscriptionPayload()).getAsJsonObject();
        NotificationEvent event = new NotificationEvent(NotificationEventType.SUBSCRIPTION_CANCEL, marketplace, creator, payload);
        SuccessResponse response = sendNotificationEvent(event, SuccessResponse.class);
        response.isSuccess();
    }

    private CancelSubscriptionPayload createCancelSubscriptionPayload() {
        CancelSubscriptionPayload payload = new CancelSubscriptionPayload();
        payload.setAccount(createAccount());
        return payload;
    }

    private <T> T sendNotificationEvent(NotificationEvent event, Class<T> responseType) {
        String eventId = UUID.randomUUID().toString();
        eventService.stageEvent(eventId, event);

        HttpGet request = new HttpGet(baseNotificationUri.toString() + "?url=" + baseEventServiceUri + eventId);

        try {
            oAuthConsumer.sign(request);
            return httpClient.execute(request, response -> handleResponse(response, responseType));
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    private <T> T handleResponse(HttpResponse response, Class<T> responseType) throws IOException {
        return gson.fromJson(new InputStreamReader(response.getEntity().getContent()), responseType);
    }
}
